# student-app

This is a demo react application for student management app. 
This example app shows how to create a Spring Boot API and CRUD (create, read, update, and delete) its data with a React app.
Prerequisites: Java 8, Node.js 8+, and Yarn. You can use npm instead of Yarn.


The example has client in student-frontend, and server in student-backend. Clone these to your machine.
Run this command to install and start the react application:

C:\Users\Your Name\student-frontend>student-frontend>npm install
C:\Users\Your Name\student-frontend>student-frontend>npm start

A new browser window will pop up with your newly created React App! If not, open your browser and type localhost:3000 in the address bar.

[The example was created using create-react-app(https://reactjs.org/docs/create-a-new-react-app.html)
The create-react-app is an officially supported way to create React applications.

If you have NPM and Node.js installed, you can create a React application by first installing the create-react-app.

 Install create-react-app by running this command in your terminal:

C:\Users\Your Name>npm install -g create-react-app
]