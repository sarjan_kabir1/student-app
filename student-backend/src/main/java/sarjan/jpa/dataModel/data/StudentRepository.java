package sarjan.jpa.dataModel.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import sarjan.jpa.dataModel.model.Student;

/**
 * @author Sarjan Kabir
 */
@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    Student findById(long id);
}