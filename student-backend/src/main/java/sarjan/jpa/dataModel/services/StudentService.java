package sarjan.jpa.dataModel.services;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sarjan.jpa.dataModel.data.StudentRepository;
import sarjan.jpa.dataModel.model.Student;

import java.util.List;

/**
 * @author Sarjan Kabir
 */

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    public Student registerStudent(Student student){

        return studentRepository.save( student);
    }

    public Student updateStudent(Student student){
        System.out.println(student.getId());
        Student oldstudent =   studentRepository.findById(student.getId());
        return studentRepository.save(student);
    }
    public Student findStudent(Long id){
        Student student =   studentRepository.findById(id);
        return student;
    }
    public List<Student> getAllStudents(){
        return (List<Student>)studentRepository.findAll();
    }
    public void deleteStudent(Long id){
        Student student =   studentRepository.findById(id);
        studentRepository.delete(student);
    }
}
