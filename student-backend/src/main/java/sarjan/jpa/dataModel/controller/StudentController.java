package sarjan.jpa.dataModel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import sarjan.jpa.dataModel.model.Student;
import sarjan.jpa.dataModel.services.StudentService;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Sarjan Kabir
 */

@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(value = "/student")
@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PostMapping(path="/register/",consumes = "application/json", produces="application/json")
    @ResponseBody
    public ResponseEntity<Student> registerStudent(@RequestBody Student student) {
        student.setDob(convertDate(student.getDob()));
        Student registeredStudent = studentService.registerStudent(student);
        return ResponseEntity.ok().body(registeredStudent);
    }

    @GetMapping(path="/{id}", produces="application/json")
    public ResponseEntity<Student> findStudent(@PathVariable Long id) {
        Student student = studentService.findStudent(id);
        return ResponseEntity.ok().body(student);
    }

    @PutMapping(path="/update/", consumes = "application/json", produces = "application/json")
    @ResponseBody
    public ResponseEntity<Student> updateStudent(@RequestBody Student student) {
        student.setDob(convertDate(student.getDob()));
        studentService.updateStudent(student);
        return ResponseEntity.ok().body(student);
    }

    @GetMapping(path="/all", produces="application/json")
    public ResponseEntity<List<Student>> getAllStudents(){
        List<Student> studentList =  studentService.getAllStudents();
        return ResponseEntity.ok().body(studentList);
    }


    @PostMapping(path="/delete/{id}", produces="application/json")
    public ResponseEntity<Boolean> delete(@PathVariable Long id) {
        studentService.deleteStudent(id);
        return ResponseEntity.ok().body(true);
    }

    public Date convertDate(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c = Calendar.getInstance();
        c.setTime(date); // Now use today date.
        c.add(Calendar.DATE, 1); // Adding 5 days
       return c.getTime();

    }

}
