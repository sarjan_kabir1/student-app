package sarjan.jpa.dataModel;

import org.hibernate.service.spi.InjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import sarjan.jpa.dataModel.model.Student;
import sarjan.jpa.dataModel.services.StudentService;

@SpringBootApplication
public class AppStarter {



    public static void main(String[] args) {
        SpringApplication.run(AppStarter.class, args);


    }
}
