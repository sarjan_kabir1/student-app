import logo from './logo.svg';
import './App.css';
import StudentList from './components/StudentList';
import Footer from './components/Footer';
import Header from './components/Header';
import {BrowserRouter as Router ,Route,Switch}from 'react-router-dom';
import CreateStudent from './components/CreateStudent';
import UpdateStudent from './components/UpdateStudent';
import ViewStudent from './components/ViewStudent';

function App() {
  return (
       <div>
        <Router>
              <Header />
                <div className="container " >
                    <Switch> 
                          <Route path = "/" exact component = {StudentList}></Route>
                          <Route path = "/students" component = {StudentList}></Route>
                          <Route path = "/add-student" component = {CreateStudent}></Route>
                          <Route path = "/view-student/:id" component = {ViewStudent}></Route> 
                          <Route path = "/update-student/:id" component = {UpdateStudent}></Route>
                    </Switch>
                </div>
              <Footer />
        </Router>
    </div>
   
  );
}

export default App;
