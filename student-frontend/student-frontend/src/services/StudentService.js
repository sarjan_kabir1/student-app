import axios from 'axios';
import StudentList from '../components/StudentList';

const STUDENT_API_BASE_URL = "http://localhost:8080/student";

class StudentService{

    getStudents(){
        return axios.get(STUDENT_API_BASE_URL+ '/all');
    }
    createStudent(student){
        return axios.post(STUDENT_API_BASE_URL +'/register/', student)
    }
    getStudentById(studentId){
        return axios.get(STUDENT_API_BASE_URL +'/'+studentId);
    }
    updateStudent(student){
        return axios.put(STUDENT_API_BASE_URL +'/update/',student);
    }
    deleteStudent(studentId){
        return axios.post(STUDENT_API_BASE_URL  +'/delete/'+studentId);
    }

}

export default new StudentService();