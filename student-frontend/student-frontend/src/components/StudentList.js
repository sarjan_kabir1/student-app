import React, { Component } from 'react';
import StudentService from '../services/StudentService';
import ViewStudent from './ViewStudent';

class StudentList extends Component {

    constructor(props) {
        super(props)

        this.state = {
            students: []
        }
        this.addStudent = this.addStudent.bind(this);
        this.editStudent = this.editStudent.bind(this);
        this.deleteStudent = this.deleteStudent.bind(this);

    }

    componentDidMount() {
        StudentService.getStudents().then((res) => {
            this.setState({ students: res.data });

        });
    }

    addStudent() {
        this.props.history.push('/add-student');
    }
    editStudent(id) {
        this.props.history.push(`/update-student/${id}`);
    }
    deleteStudent(id) {
        StudentService.deleteStudent(id).then(res => {
            this.setState({ students: this.state.students.filter(student => student.id !== id) });
        })
        .catch(error =>{
            alert("Cannot delete student.");
        })

    }
    viewStudent(id) {

        this.props.history.push(`/view-student/${id}`)
    }
    render() {
        return (
            <div>
                <br></br>
                <div className="row">
                    <div className="col-md-12">
                    <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">List of Students</li>
                    </ol>
                </nav>
                    </div>
                
                </div>
                <br></br>
                <div className="row">
                <div className="col-md-12">
                    <button className="btn btn-primary" onClick={this.addStudent}>
                        Add Student
                   </button>
                   </div>
                   </div>
                   <br></br>
                <div className="row table-responsive">
                    <div className="col-md-12">
                        <table className="table table-striped table-bordered table-sm">
                        <thead>
                            <tr>
                                <th> Name</th>
                                <th> Phone</th>
                                <th> Date of Birth</th>
                                <th> Email</th>
                                <th> University</th>
                                <th> Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.students.map(
                                    student =>
                                        <tr key={student.id}>
                                            <td>{student.name}</td>
                                            <td>{student.phoneNumber}</td>
                                            <td>{student.dob}</td>
                                            <td>{student.email}</td>
                                            <td>{student.university}</td>
                                            <td>
                                                <button onClick={() => this.editStudent(student.id)} className="btn btn-outline-primary btn-sm">Update </button>
                                                <button style={{ marginLeft: "10px" }} onClick={() => this.deleteStudent(student.id)} className="btn btn-outline-primary btn-sm">Delete </button>
                                                <button style={{ marginLeft: "10px" }} onClick={() => this.viewStudent(student.id)} className="btn btn-outline-primary btn-sm">View </button>
                                            </td>

                                        </tr>
                                )
                            }
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default StudentList;