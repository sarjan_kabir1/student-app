import React, { Component } from 'react';
import StudentService from '../services/StudentService';
import StudentList from './StudentList';

class UpdateStudent extends Component 
    {

        constructor(props) {
            super(props)
    
            this.state = {
                
                id: this.props.match.params.id,
                name: '',
                phoneNumber:'',
                dob: '',
                email: '',
                university:'',
    
            }
            this.handleChange = this.handleChange.bind(this);
            
             this.updateStudent = this.updateStudent.bind(this);
        }
        componentDidMount(){
            StudentService.getStudentById(this.state.id).then( (res) =>{
                const student = res.data;
                this.setState({name: student.name,
                    phoneNumber: student.phoneNumber,
                    dob : student.dob,
                    email: student.email,
                    university: student.university
                });
            });
        }
    
        updateStudent = (e) => {
            e.preventDefault();
            let student = { id: this.state.id, name: this.state.name,phoneNumber:this.state.phoneNumber, dob: this.state.dob, email: this.state.email, university:this.state.university};
            console.log('student => ' + JSON.stringify(student));

            StudentService.updateStudent(student).then(res => {
                this.props.history.push('/students');
            })
            .catch(error =>{
                alert("Cannot update student.");
            })

        }
         handleChange =(event) =>{

            this.setState({[event.target.name]: event.target.value});
          }
          cancel(){
            this.props.history.push('/students');
        }
        render() {
            return (
                <div>
                <br></br>
                       <div className = "container">
                            <div className = "row">
                                <div className = "card col-md-6 offset-md-3 offset-md-3">
                                    <h3 className ="text-center">Update Student</h3>
                                    <div className = "card-body">
                                        <form>
                                            <div className = "form-group">
                                                <label> Name: </label>
                                                <input placeholder="Name" name="name" className="form-control" 
                                                    value={this.state.name} onChange={this.handleChange}/>
                                            </div>
                                            <div className = "form-group">
                                                <label> Phone: </label>
                                                <input placeholder="Phone Number" name="phoneNumber" className="form-control" 
                                                    value={this.state.phoneNumber} onChange={this.handleChange}/>
                                            </div>
                                            <div className = "form-group">
                                                <label> Date of Birth: </label>
                                                <input placeholder="Date of Birth (YYYY-MM-DD)" name="dob" type = "date" className="form-control" 
                                                    value={this.state.dob} onChange={this.handleChange}/>
                                            </div>
                                            <div className = "form-group">
                                                <label>Email address: </label>
                                                <input placeholder="Email address" name="email" className="form-control" 
                                                    value={this.state.email} onChange={this.handleChange}/>
                                            </div>
                                            <div className = "form-group">
                                                <label> University: </label>
                                                <input placeholder="University" name="university" className="form-control" 
                                                    value={this.state.university} onChange={this.handleChange}/>
                                            </div>
    
                                            <button className="btn btn-success" onClick={this.updateStudent}>Save</button>
                                            <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
    
                       </div>
                </div>
            );
        }
    }
    

export default UpdateStudent;