import React, { Component } from 'react';
import StudentService from '../services/StudentService';

class ViewStudent extends Component {
    constructor(props) {
        
        super(props)

        this.state = {
            id: this.props.match.params.id,
            student: {}
        }
    }

    componentDidMount(){
        console.log(this.state)
        StudentService.getStudentById(this.state.id).then( res => {
            this.setState({student: res.data});
        })
        .catch(error =>{
            alert("Cannot load student.");
        })
    }

    render() {
        return (
            <div >
                <br></br>
                
                <div class="card col-md-6 offset-md-3 ">
                    <div class="card-header">
                  <u><b>Student Details</b> </u> 
                    </div>
                    <div className = "card-body">
                        <div className = "row">
                            <div className ="col-md-4"><label > <b>Student Name: </b> </label> </div>
                            <div className ="col-md-6"> { this.state.student.name }</div>
                        </div>
                        <div className = "row">
                        <div className ="col-md-4"><label > <b>Phone Number: </b> </label> </div>
                            <div  className ="col-md-6"> { this.state.student.phoneNumber }</div>
                        </div>
                        <div className = "row">
                        <div className ="col-md-4"><label > <b>Email: </b> </label> </div>
                            <div  className ="col-md-6"> { this.state.student.email }</div>
                        </div>
                        <div className = "row">
                        <div className ="col-md-4"><label > <b>Date of Birth: </b> </label> </div>
                            <div  className ="col-md-6"> { this.state.student.dob }</div>
                        </div>
                        <div className = "row">
                        <div className ="col-md-4"><label > <b>University </b> </label> </div>
                            <div  className ="col-md-6"> { this.state.student.university }</div>
                        </div>
                    </div>
                    </div>

            </div>
        )
    }
}

export default ViewStudent;