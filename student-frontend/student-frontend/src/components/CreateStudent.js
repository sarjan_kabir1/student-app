import React, { Component } from 'react';
import StudentService from '../services/StudentService';

class CreateStudent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            // step 2
            id: this.props.match.params.id,
            name: '',
            phoneNumber: '',
            dob: '',
            email: '',
            university: '',

        }
        this.handleChange = this.handleChange.bind(this);
        // this.changeLastNameHandler = this.changeLastNameHandler.bind(this);
        this.saveStudent = this.saveStudent.bind(this);
    }

    saveStudent = (e) => {
        e.preventDefault();
        let student = { name: this.state.name, phoneNumber: this.state.phoneNumber, dob: this.state.dob, email: this.state.email, university: this.state.university };
        console.log('student => ' + JSON.stringify(student));

        // // step 5
        // if(this.state.id === '_add'){
        StudentService.createStudent(student).then(res => {
            this.props.history.push('/students');
        })
        .catch(error =>{
            alert(error.message);
        })
        // }else{
        //     EmployeeService.updateEmployee(employee, this.state.id).then( res => {
        //         this.props.history.push('/employees');
        //     });
        // }
    }
    handleChange = (event) => { this.setState({ [event.target.name]: event.target.value });
    }
    cancel() {
        this.props.history.push('/students');
    }
    render() {
        return (
            <div>
                <br></br>
                <div className="container">
                    <div className="row">
                        <div className="card col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">Add Student</h3>
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label> Name: </label>
                                        <input placeholder="Name" name="name" className="form-control"
                                            value={this.state.name} onChange={this.handleChange} />
                                    </div>
                                    <div className="form-group">
                                        <label> Phone: </label>
                                        <input placeholder="Phone Number" name="phoneNumber" className="form-control"
                                            value={this.state.phoneNumber} onChange={this.handleChange} />
                                    </div>
                                    <div className="form-group">
                                        <label> Date of Birth: </label>
                                        <input placeholder="Date of Birth (YYYY-MM-DD)" name="dob" type="date"
                                            className="form-control"
                                            value={this.state.dob} onChange={this.handleChange} />
                                    </div>
                                    <div className="form-group">
                                        <label>Email address: </label>
                                        <input placeholder="Email address" name="email" className="form-control"
                                            value={this.state.email} onChange={this.handleChange} />
                                    </div>
                                    <div className="form-group">
                                        <label> University: </label>
                                        <input placeholder="University" name="university" className="form-control"
                                            value={this.state.university} onChange={this.handleChange} />
                                    </div>

                                    <button className="btn btn-success" onClick={this.saveStudent}>Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{ marginLeft: "10px" }}>Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

export default CreateStudent;